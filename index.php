<?php

require_once "./diccionario.php";

// string de 32 caracteres que contiene 
// una combinación de 2 letras
$cadenas = [
    "1ee7b2c95024bf2a3a5b645676875f72", //1  -  gd
    "af97c725eca5ea2e7ae4a6b18dd9f608", //2  -  el
    "aaf180b66f6282f65168c30c1c5e5466", //3  -  eo
    "0aaa8fb0c5b4fa52c0d06ed55d02fcc9", //4  -  n@
    "5e35b5a6a854db008cb8a677dfff030b", //5  -  de
    "35306f43e19f6826a5df7937e71d228c", //6  -  nt
    "b2917b47913c83618903a4e564822836", //7  -  al
    "34e4cf70388e9477677aa3013494ba1c", //8  -  ia
    "3d116007775d60a0d5781d2e35d747b5", //9  -  .c
    "dece2e0e3d79d272e40c8c66555f5525"  //10  - om
];

echo "<h1>correo: gdeleon@dentalia.com</h1>";


foreach ($diccionario as $letra) {
    foreach ($letra as $combinacion) {
        foreach ($cadenas as $str) {
            if (s_md5($combinacion) === $str) {
                echo "combinacion: <b>", $combinacion, "</b><br/>";
                echo "encriptado: ", $str, "<br/>";
                echo "--------------------------------------<br/>";
            }
        }
    }
}

function s_md5($str) {
    $encripta = md5($str) . $str . md5($str);
    return md5($encripta);
}
